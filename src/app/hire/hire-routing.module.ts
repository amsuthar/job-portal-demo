import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HireComponent } from './hire.component';
import { HireStep1Component } from './hire-step1/hire-step1.component';

const routes: Routes = [
  {
    path: '',
    component: HireComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'hire', pathMatch: 'full' },
      { path: 'hire', component: HireStep1Component },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HireRoutingModule { }
