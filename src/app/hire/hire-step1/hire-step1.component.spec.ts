import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HireStep1Component } from './hire-step1.component';

describe('HireStep1Component', () => {
  let component: HireStep1Component;
  let fixture: ComponentFixture<HireStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HireStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HireStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
