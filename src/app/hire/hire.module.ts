import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HireRoutingModule } from './hire-routing.module';
import { HireComponent } from './hire.component';
import { HireStep1Component } from './hire-step1/hire-step1.component';
import { MaterialModule } from '../material-module';

@NgModule({
  declarations: [HireComponent, HireStep1Component],
  imports: [
    CommonModule,
    HireRoutingModule,
    MaterialModule
  ]
})
export class HireModule { }
