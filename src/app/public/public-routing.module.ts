import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';
import { AuthGuard } from '../core/services/interceptor';
import { FirstPageComponent } from './first-page/first-page.component';
// import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'first-page', pathMatch: 'full' },
      { path: 'first-page', component: FirstPageComponent },

      //   // { path: 'resetpasswordemail', component: ResetPasswordEmailComponent },
      //   // { path: 'resetpassword', component: ResetPasswordComponent },
      //   { path: '**', redirectTo: 'login', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
