import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { FirstPageComponent } from './first-page/first-page.component';
import { MaterialModule } from '../material-module';


@NgModule({
  declarations: [PublicComponent, FirstPageComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    MaterialModule
  ]
})
export class PublicModule { }
